const fs = require('fs').promises
const express = require('express')
const morgan = require('morgan')
const app = express()

app.use(express.json())
app.use(morgan('tiny'))

app.post('/api/files', async (req, res) => {
	let data = req.body,
			filename = data.filename,
			content = data.content,
			regex = /(\.(log|txt|json|yaml|xml|js)$)/i

	try {
		if (regex.test(filename) !== true) {
			res.status(400)
			throw { message: "Please specify 'filename' parameter" }
		} else if (content === undefined) {
			res.status(400)
			throw { message: "Please specify 'content' parameter" }
		}

		await fs.writeFile(`./api/files/${filename}`, `${content}`, 'utf8')
		res.send({ message: 'File created successfully' })

	}
	catch (err) {
		if (res.statusCode == 500) err = { message: 'Server error' }
		res.send(err)
	}
})


app.get('/api/files', async (req, res) => {
	try {
		await fs.access('./api/files')
		let files = await fs.readdir('./api/files')
		res.send({ message: 'Success', files: files })
	}
	catch(err) {
		if (res.statusCode == 500) res.send({ message: 'Server error' })
		res.status(400).send({ "message": "Client error" })
	}
})


app.get('/api/files/:filename', async (req, res) => {
	try {
		let filename = req.params.filename
		let files = await fs.readdir('./api/files')

		if (files.includes(filename) === false) {
			res.status(400)
			throw { "message": "No file with 'notes.txt' filename found" }
		}

		let regex = /((log|txt|json|yaml|xml|js)$)/gi
		let stats = await fs.stat(`./api/files/${filename}`)
		let info = {
			message: 'Success',
			filename: filename
		}
	
		info.content = await fs.readFile(`./api/files/${filename}`, 'utf8')
		info.extension = `${filename.match(regex)}`
		info.uploadedDate = stats.birthtime
		
		res.send(info);
	}
	catch (err) {
		if (res.statusCode == 500) return res.send({ message: 'Server error' })
		res.send(err)
	}
})

app.listen(8080)